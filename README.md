# Bibliothèque

<div align="center">
<img width="500" height="400" src="bibliotheque.jpg">
</div>

## Description du projet

Application web réalisée avec Java EE en LP MIAR à l'IUT de Nantes dans le cadre du module "Technologies Web coté serveur" durant l'année 2018-2019. \
Elle permet de gérer un catalogue de livres.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un intervenant profesionnel :
- Laurent GUERIN : lgu.univ@gmail.com.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Technologies Web coté serveur" de la LP MIAR.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu en 2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java EE ;
- Eclipse ;
- Maven ;
- Servlet ;
- JSP ;
- JSTL ;
- E.L. ;
- Tomcat.

## Objectifs

La finalité de ce projet est de pouvoir consulter et gérer un catalogue de livres.