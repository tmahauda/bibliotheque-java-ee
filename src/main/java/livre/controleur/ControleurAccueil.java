package livre.controleur;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ControleurAccueil implements Controleur
{
	public String process(HttpServletRequest request) throws ServletException, IOException
	{
		HttpSession session = request.getSession(false);
		
		if(session != null)
		{
			Object livres = session.getAttribute("livresSelection");
			String vider = request.getParameter("vider");
			
			if(livres != null && vider != null)
			{
				//On détruit la sélection des livres à travers la session
				//Détruit la variable livresSelection
				session.invalidate();
			}
		}
		
		
		return "accueil.jsp";
	}
}
