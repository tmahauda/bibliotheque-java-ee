package livre.controleur;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Action
 */
public class Action extends HttpServlet 
{
	
	private static final long serialVersionUID = 1L;
	private HashMap<String, Controleur> actions;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Action() 
    {
        super();
        this.actions = new HashMap<String, Controleur>();
        this.actions.put("accueil", new ControleurAccueil());
    	this.actions.put("catalogue", new ControleurCatalogue());
    	this.actions.put("filtration", new ControleurFiltration());
    	this.actions.put("modification", new ControleurModification());
    	this.actions.put("selection", new ControleurSelection());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		process(request, response);
	}
	
    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		process(request, response);
	}
	
    /**
     * Méthode qui effectue un traitement sur la page demandé avant de l'envoyer au client
     * - Récupère le nom de l'action dans la requete
     * - Récupèe le controleur de la page
     * - Initialise les services de la page grâce au controleur
     * - Redirige le client vers la page demandé
     * @param request provenant du client
     * @param response à envoyer au client
     * @throws ServletException
     * @throws IOException
     */
    private void process(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException 
    {
        String actionName = this.getActionName(request);
        System.out.println("Get action control : " + actionName);
        
        System.out.println("Process action control : " + actionName);
        Controleur actionControl = this.getActionControl(actionName);
        String dispatch = actionControl.process(request);

        System.out.println("Dispatch to " + dispatch);
        this.dispatcher(request, response, "/WEB-INF/jsp/"+dispatch);
    }
    
    
    /**
     * Méthode qui récupère le nom de l'action issu de la requête après le nom du routage
     * @param request provenant du client
     * @return l'action à effectuer
     * @throws ServletException
     * @throws IOException
     */
    private String getActionName(HttpServletRequest request) throws ServletException, IOException
    {
    	String path = request.getPathInfo();
    	System.out.println("Path " + path);
    	
    	if(path == null || path.equals("") || path.equals("/"))
    	{
    		path = "/accueil";
    		System.out.println("Path welcom " + path);
    	}
    	else
    	{
    		path = path.toLowerCase().trim();
    	}
    	
    	return path.substring(1);
    }
    
    /**
     * Méthode qui récupère le controleur de l'action grâce au nom de l'action depuis une HashMap<action, controleur>
     * @param actionName le nom du controleur
     * @return le controleur de l'action
     * @throws ServletException
     * @throws IOException
     */
    private Controleur getActionControl(String actionName) throws ServletException, IOException 
    {
    	if(this.actions.containsKey(actionName))
    	{
    		return this.actions.get(actionName);
    	}
    	else
    	{
    		throw new IllegalStateException("Cannot find action control " + actionName);
    	}
    }
    
    /**
     * Méthode qui effectue une redirection vers la page demandée
     * @param request provenant du client
     * @param response à envoyer au client
     * @param destination le chemin de la page à charger au client
     * @throws ServletException
     * @throws IOException
     */
    private void dispatcher(HttpServletRequest request, HttpServletResponse response, String destination) 
    		throws ServletException, IOException 
	{
		    RequestDispatcher rd = request.getRequestDispatcher(destination);
		    
		    if (rd != null) 
		    {
		        rd.forward(request, response); 
		    }
		    else 
		    {
		    	throw new IllegalStateException("Cannot get RequestDispatcher");
		    }
	}
}
