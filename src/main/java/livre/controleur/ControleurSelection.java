package livre.controleur;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import livre.modele.Catalogue;
import livre.modele.Livre;

public class ControleurSelection implements Controleur
{
	public String process(HttpServletRequest request) throws ServletException, IOException
	{
		String newSelection = request.getParameter("new");
		
		//Si on a fait une nouvelle selection de livre
		if(newSelection != null)
		{
			Catalogue catalogue = Catalogue.getCatalogue();
			
			if(catalogue != null)
			{
				Vector<Livre> livres = null;
				
				if(this.filter(request))
				{
					try
					{
						double min = this.critere("min", request);
						double max = this.critere("max", request);
						livres = catalogue.filtre(min, max);
						
						if(livres != null)
						{
							HttpSession session = request.getSession(true);
							session.setAttribute("livresSelection", livres);
							session.setAttribute("min", min);
							session.setAttribute("max", max);
							
							return "selection.jsp";
						}
						else
						{
							request.setAttribute("error", "Livres non disponibles");
							return "erreur.jsp";
						}
					}
					catch(NumberFormatException e)
					{
						request.setAttribute("error", "Min et/ou max non renconnu");
						return "erreur.jsp";
					}
				}
				else
				{
					request.setAttribute("error", "Min et/ou max non renseignés");
					return "erreur.jsp";
				}
			}
			else
			{
				request.setAttribute("error", "Catalogue non disponible");
				return "erreur.jsp";
			}
		}
		//Sinon on renvoit la page avec la selection déjà effectué
		else
		{
			return "selection.jsp";
		}
	}
	
	private double critere(String critere, HttpServletRequest request) throws NumberFormatException
	{
		try
		{
			return Double.parseDouble(request.getParameter(critere));
		}
		catch(NumberFormatException e)
		{
			throw e;
		}
	}
	
	private boolean filter(HttpServletRequest request)
	{
		String min = request.getParameter("min");
		String max = request.getParameter("max");
		
		return min != null && max != null;
	}
}
