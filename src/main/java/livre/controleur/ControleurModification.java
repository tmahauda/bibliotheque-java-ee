package livre.controleur;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import livre.modele.Catalogue;
import livre.modele.Livre;

public class ControleurModification implements Controleur
{
	public String process(HttpServletRequest request) throws ServletException, IOException
	{
		String strId = request.getParameter("id");
		
		if(strId != null && strId != "")
		{
			try 
			{
				int intId = Integer.valueOf(strId);
				Catalogue catalogue = Catalogue.getCatalogue();
				
				if(catalogue != null)
				{
					Livre livre = catalogue.getLivre(intId);
					
					if(livre != null)
					{
						request.setAttribute("livre", livre);
						return "modification.jsp";
					}
					else
					{
						request.setAttribute("error", "Livre non disponible : " + intId);
						return "erreur.jsp";
					}
				}
				else
				{
					request.setAttribute("error", "Catalogue non disponible");
					return "erreur.jsp";
				}
			}
			catch(NumberFormatException e)
			{
				request.setAttribute("error", "ID non reconnu : " + strId);
				return "erreur.jsp";
			}
		}
		else
		{
			request.setAttribute("error", "ID non renseigné");
			return "erreur.jsp";
		}
	}
}
