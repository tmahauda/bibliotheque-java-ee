package livre.controleur;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public interface Controleur 
{
	/**
	 * Méthode qui permet d'effectuer un traitement sur la requete
	 * en fonction de la page demandée
	 * @param request à traiter
	 * @throws ServletException
	 * @throws IOException
	 */
	public String process(HttpServletRequest request) 
			throws ServletException, IOException;
}
