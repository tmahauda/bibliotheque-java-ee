package livre.controleur;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class ControleurFiltration implements Controleur
{
	public String process(HttpServletRequest request) throws ServletException, IOException
	{
		return "filtration.jsp";
	}
}
