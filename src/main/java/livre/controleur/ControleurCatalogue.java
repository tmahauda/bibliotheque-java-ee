package livre.controleur;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import livre.modele.Catalogue;
import livre.modele.Livre;

public class ControleurCatalogue implements Controleur
{
	public String process(HttpServletRequest request) throws ServletException, IOException
	{
		Catalogue catalogue = Catalogue.getCatalogue();
		
		if(catalogue != null)
		{
			Vector<Livre> livresCatalogue = catalogue.filtre();

			if(livresCatalogue != null)
			{
				request.setAttribute("livresCatalogue", livresCatalogue);
				return "catalogue.jsp";
			}
			else
			{
				request.setAttribute("error", "Livres non disponible");
				return "erreur.jsp";
			}
		}
		else
		{
			request.setAttribute("error", "Catalogue non disponible");
			return "erreur.jsp";
		}
	}
}
