<%@ include file="/WEB-INF/jspf/header.jspf" %>
<ul>
	<li><a href="${pageContext.request.contextPath}/action/catalogue">Lister tous les livres du catalogue</a></li>
	<li><a href="${pageContext.request.contextPath}/action/filtration">Filtrer les livres en fonction du prix</a></li>
	<c:if test="${sessionScope.livresSelection != null}">
		<li><a href="${pageContext.request.contextPath}/action/selection">Lister la s�lection de livres</a></li>
		<li><a href="${pageContext.request.contextPath}/action/accueil?vider=true">Vider la s�lection de livres</a></li>
	</c:if>		
</ul>  
<%@ include file="/WEB-INF/jspf/footer.jspf" %>