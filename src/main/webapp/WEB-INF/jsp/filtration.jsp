<%@ include file="/WEB-INF/jspf/header.jspf" %>
<form method="get" action="${pageContext.request.contextPath}/action/selection">
	<table>
		<tr>
			<td><label for="price">Prix</label></td>
		</tr>
		<tr>
			<td><label for="min">Mini</label></td>
			<td><input type="text" id="min" name="min" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td><label for="max">Maxi</label></td>
			<td><input type="text" id="max" name="max" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td><input type="hidden" id="new" name="new" value="new"></td>
			<td><input type="submit" value="Filtrer"/></td>
		</tr>
	</table>
</form>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>