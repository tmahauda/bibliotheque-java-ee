<%@ include file="/WEB-INF/jspf/header.jspf" %>
<h1>S�lection de livres dont le prix est compris entre ${sessionScope.min} et ${sessionScope.max}</h1>
<table border="1">
	<tr>
		<th> ID </th>
		<th> Titre </th>
		<th> Auteur </th>
		<th> Prix </th>
		<th>  <th>
	</tr>
	<c:forEach items="${sessionScope.livresSelection}" var="livre">
		<tr>
			<td> ${livre.id} </td>
			<td> ${livre.titre} </td>
			<td> ${livre.auteur} </td>
			<td> ${livre.prix} </td>
			<td> <a href="${pageContext.request.contextPath}/action/modification?id=${livre.id}">Modifier</a> </td>
		</tr>
	</c:forEach>
</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>