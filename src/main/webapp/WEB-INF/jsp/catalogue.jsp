<%@ include file="/WEB-INF/jspf/header.jspf" %>
<h1>Liste de tous les livres du catalogue </h1>
<table border="1">
	<tr>
		<th> ID </th>
		<th> Titre </th>
		<th> Auteur </th>
		<th> Prix </th>
		<th>  <th>
	</tr>
	<c:forEach items="${requestScope.livresCatalogue}" var="livre">
		<tr>
			<td> ${livre.id} </td>
			<td> ${livre.titre} </td>
			<td> ${livre.auteur} </td>
			<td> ${livre.prix} </td>
			<td> <a href="${pageContext.request.contextPath}/action/modification?id=${livre.id}">Modifier</a> </td>
		</tr>
	</c:forEach>
</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>