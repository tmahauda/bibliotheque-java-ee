<%@ include file="/WEB-INF/jspf/header.jspf" %>
<h1>Modification du livre</h1>
<form method="post" action="${pageContext.request.contextPath}/action/accueil">
	<table>
		<tr>
			<td><label for="id">Id</label></td>
			<td><input id="id" value="${requestScope.livre.id}" readonly="readonly"/></td>
		</tr>
		<tr>
			<td><label for="title">Titre</label></td>
			<td><input type="text" id="title" name="title" value="${requestScope.livre.titre}" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td><label for="author">Auteur</label></td>
			<td><input type="text" id="author" name="author" value="${requestScope.livre.auteur}" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td><label for="author">Prix</label></td>
			<td><input type="text" id="price" name="price" value="${requestScope.livre.prix}" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td><input type="submit" value="Enregistrer"/></td>
		</tr>
	</table>
</form>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>